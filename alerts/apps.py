# -*- coding: utf-8 -*-
" custom options for admin Alerts "

from django.apps import AppConfig


class AlertsConfig(AppConfig):
    name = 'alerts'
    verbose_name = "Alertas"
