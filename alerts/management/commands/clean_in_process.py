# -*- coding: utf-8 -*-

from django.core.management import BaseCommand
from alerts.models import Resume


class Command(BaseCommand):
    help = 'clean all resumes in process'

    def handle(self, *args, **options):
        Resume.objects.in_process().update(result=Resume.NOT_SENT)
