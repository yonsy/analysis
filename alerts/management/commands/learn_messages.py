# -*- coding: utf-8 -*-


from django.core.management import BaseCommand
from django.conf import settings
from alerts.models import Client, Message

from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob

import pickle


def learn_messages(client, classiffied):
    learned_messages = client.message_set.learned(classiffied).count()
    remainder = settings.LEARN_MAX - learned_messages
    if remainder > 0:
        messages = client.message_set.not_learned(classiffied)[:remainder]
        train = []
        if messages.count() > 0:
            for message in messages:
                resume_body = unicode(''.join([
                    message.title, '. ', message.body]))
                try:
                    traduced = TextBlob(resume_body)
                    traduced = traduced.translate(to='en')
                    traduced = unicode(traduced)
                except:
                    traduced = resume_body
                train.append((
                    traduced,
                    unicode(message.classiffied)
                ))
                message.learned = True
                message.save()
            filename = ''.join([
                'learn', '/', client.code, '/', 'learn.tb'])
            try:
                with open(filename, 'rb') as f:
                    cl = pickle.load(f)
                cl.update(train)
            except:
                cl = NaiveBayesClassifier(train)

            with open(filename, 'wb') as f:
                pickle.dump(cl, f)


class Command(BaseCommand):
    help = 'import, check and learn from clients in application'

    def handle(self, *args, **options):
        clients = Client.objects.all()
        for client in clients:
            learn_messages(client, Message.NEGATIVE)
            learn_messages(client, Message.POSITIVE)
            learn_messages(client, Message.NEUTRAL)
