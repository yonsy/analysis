# -*- coding: utf-8 -*-


from django.core.management import BaseCommand
from django.conf import settings
from alerts.models import Resume

from datetime import datetime
from textblob import TextBlob
from setproctitle import setproctitle
from psutil import process_iter
from time import sleep
from random import randint

import requests
import os
import pickle


url = [
    'http://framework.ipnoticias.com/DPFramework/API/',
    'ArchivoComun/EditaTipoTono',
]
url = ''.join(url)


def get_learn_result(client, expresion):
    filename = ''.join(['learn', '/', client, '/', 'learn.tb'])
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            cl = pickle.load(f)
        result = cl.classify(expresion)
    else:
        try:
            text = TextBlob(expresion)
            text = text.translate()
            evaluation = text.polarity * text.subjectivity
            if evaluation > 0.33:
                result = Resume.POSITIVE
            elif evaluation > -0.33:
                result = Resume.NEUTRAL
            else:
                result = Resume.NEGATIVE
        except:
            result = Resume.NEUTRAL

    return result


def evaluate_and_save(resume, params):
    body = requests.get(url, params)
    resume.result = body.content
    if resume.result == 'true':
        resume.sent_at = datetime.now()
    else:
        resume.sent_at = None
    resume.save()


def is_running(proc_name):
    result = False
    for proc in process_iter():
        if proc.as_dict(attrs=['name'])['name'] == proc_name:
            result = True
    return result


def is_sleep(proc_name):
    result = False
    for proc in process_iter():
        if proc.as_dict(attrs=['name', 'status']) ==\
                {'name': proc_name, 'status': 'sleeping'}:
            result = True
    return result


def are_sleep():
    result = False
    for segment in xrange(1, settings.SEGMENTS+1):
        proc_name = u''.join(['resume', segment.__str__()])
        for proc in process_iter():
            if proc.as_dict(attrs=['name', 'status']) ==\
                    {'name': proc_name, 'status': 'sleeping'}:
                result = True
    return result


def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)


class Command(BaseCommand):
    help = 'sent resumes async'

    def handle(self, *args, **options):
        proc_filename = ''
        segment = 0

        while segment < settings.SEGMENTS:
            segment += 1
            proc_name = u''.join(['resume', segment.__str__()])
            if (not is_running(proc_name)) and (not are_sleep()):
                proc_filename = ''.join(['/', 'tmp', '/', proc_name])
                touch(proc_filename)
                setproctitle(proc_name)
                break
            elif is_sleep(proc_name):
                break

        while os.path.exists(proc_filename):
            resumes = Resume.objects.not_sent()
            if resumes.count() == 0:
                break

            for resume in resumes:
                if Resume.objects.get(id=resume.id).result == Resume.NOT_SENT:
                    Resume.objects.filter(
                        id=resume.id
                    ).update(
                        result=Resume.IN_PROCESS)
                    try:
                        traduced = TextBlob(resume.body)
                        traduced = traduced.translate(to='en')
                        traduced = unicode(traduced)
                    except:
                        traduced = resume.body
                    resume.classiffied = get_learn_result(
                        resume.client.code, traduced)
                    params = {
                        'cac': str(resume.code),
                        'ctt': resume.classiffied,
                        'au': resume.security,
                    }
                    evaluate_and_save(resume, params)
            sleep(randint(0, 60))
