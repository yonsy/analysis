# -*- coding: utf-8 -*-

from django.core.management import BaseCommand
from alerts.models import Client


class Command(BaseCommand):
    help = 'compare positive, negative and neutral messages in clients'

    def compare_messages(self, client):

        pos = client.message_set.positive().count()
        neg = client.message_set.negative().count()
        neu = client.message_set.neutral().count()

        if neg < pos and neg < neu:
            default = 'NEGATIVE'
            value = neg
        elif pos < neg and pos < neu:
            default = 'POSITIVE'
            value = pos
        else:
            default = 'NEUTRAL'
            value = neu

        self.stdout.write(
            u'client: {} pos: {} neg: {} neu: {} default: {} value {}'.
            format(client.name, pos, neg, neu, default, value))

    def handle(self, *args, **options):
        clients = Client.objects.all()
        for client in clients:
            self.compare_messages(client)
