# -*- coding: utf-8 -*-

from django.core.management import BaseCommand
from django.db import connection, connections
from alerts.models import Client, Message

from datetime import datetime, date
import requests
import json


def make_sure_mysql_usable():
    if connection.connection and not connection.is_usable():
        del connections._connections.default


class Command(BaseCommand):
    help = 'load alerts from web agent'
    url = [
        'http://200.173.1.13',
        '/IPAnalisis/Reportes/ListaAlertasTonos.aspx',
    ]
    url = ''.join(url)

    start_date = date(day=1, month=1, year=2014)
    end_date = date.today()

    def handle(self, *args, **options):

        make_sure_mysql_usable()
        clients = Client.objects.all()

        for client in clients:
            params = {
                'c': client.code,
                'fi': self.start_date.strftime('%d/%m/%Y'),
                'ff': self.end_date.strftime('%d/%m/%Y'),
            }
            req = requests.get(self.url, params=params)
            content = req.content
            for asc_code in range(0, 32):
                content = content.replace(chr(asc_code), ' ')

            try:
                messages = json.loads(content)
            except ValueError:
                messages = []

            if messages.__len__() > 1:
                for web_message in messages:
                    try:
                        make_sure_mysql_usable()
                        message = Message.objects.get(
                            code=long(web_message['c']))
                    except Message.DoesNotExist:
                        make_sure_mysql_usable()
                        message = Message()
                        message.client = client
                        message.code = web_message['c']
                        message.title = web_message['t']
                        message.body = web_message['r']
                        message.learned = False
                        message.date = datetime.strptime(
                            web_message['fe'],
                            '%d/%m/%Y %H:%M:%S').date()
                        message.time = datetime.strptime(
                            web_message['fe'],
                            '%d/%m/%Y %H:%M:%S').time()
                        message.classiffied = web_message['to']
                        message.save()
                    except:
                        pass
