# -*- coding: utf-8 -*-


from django.core.management import BaseCommand
from alerts.models import Resume

from datetime import datetime
from textblob import TextBlob

import requests
import os
import pickle


url = [
    'http://framework.ipnoticias.com/DPFramework/API/',
    'ArchivoComun/EditaTipoTono',
]
url = ''.join(url)


def get_learn_result(client, expresion):
    filename = ''.join(['learn', '/', client, '/', 'learn.tb'])
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            cl = pickle.load(f)
        result = cl.classify(expresion)
    else:
        try:
            text = TextBlob(expresion)
            text = text.translate()
            evaluation = text.polarity * text.subjectivity
            if evaluation > 0.33:
                result = Resume.POSITIVE
            elif evaluation > -0.33:
                result = Resume.NEUTRAL
            else:
                result = Resume.NEGATIVE

        except:
            result = Resume.NEUTRAL

    return result


def evaluate_and_save(resume, params):
    body = requests.get(url, params)
    resume.result = body.content
    if resume.result == 'true':
        resume.sent_at = datetime.now()
    resume.save()


class Command(BaseCommand):
    help = 'sent resumes that failed in normal tasks'

    def handle(self, *args, **options):
        resumes = Resume.objects.not_sent()
        for resume in resumes:
            process_resume = Resume.objects.get(id=resume.id)
            if process_resume.result == u'false':
                process_resume.result = u'process'
                process_resume.save()
                try:
                    traduced = TextBlob(resume.body)
                    traduced = traduced.translate(to='en')
                    traduced = unicode(traduced)
                except:
                    traduced = resume.body

                resume.classiffied = get_learn_result(
                    resume.client.code, traduced)
                params = {
                    'cac': str(resume.code),
                    'ctt': resume.classiffied,
                    'au': resume.security,
                }
                evaluate_and_save(resume, params)
