# -*- coding: utf-8 -*-

from django.core.management import BaseCommand
from alerts.models import Client

import requests
import os

url = [
    'http://framework.ipnoticias.com/dpframework/api/',
    'empresa/ListaEmpresaServicioEstado',
]
url = ''.join(url)


params = {
    'c': 'FWUDQiJBgJFTA7HCQvCg6g=='
}


class Command(BaseCommand):
    help = 'import and check clients in application'

    def handle(self, *args, **options):
        xml_doc = requests.get(url, params=params)
        json_doc = xml_doc.json()
        for client in json_doc:
            try:
                Client.objects.get(name=client['n'], code=client['ce'])
            except Client.DoesNotExist:
                new_client = Client()
                new_client.name = client['n']
                new_client.code = client['ce']
                new_client.state = client['eb']
                new_client.service = client['esb']
                new_client.save()

        clients = Client.objects.all()
        for client in clients:
                path = ''.join(['learn', '/', client.code])
                if not os.path.exists(path):
                    os.makedirs(path)
