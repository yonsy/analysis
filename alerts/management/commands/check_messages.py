# -*- coding: utf-8 -*-


from django.core.management import BaseCommand
from alerts.models import Client
from datetime import date

import requests
import json


class Command(BaseCommand):
    help = 'load alerts from web agent'
    url = ''.join(['http://200.173.1.13',
                   '/IPAnalisis/Reportes/ListaAlertasTonos.aspx', ])
    start_date = date(day=1, month=1, year=2014)
    end_date = date(day=31, month=12, year=2018)

    def handle(self, *args, **options):
        clients = Client.objects.all()

        for client in clients:
            params = {
                'c': client.code,
                'fi': self.start_date.strftime('%d/%m/%Y'),
                'ff': self.end_date.strftime('%d/%m/%Y'),
            }
            req = requests.get(self.url, params=params)
            content = req.content
            for asc_code in range(0, 32):
                content = content.replace(chr(asc_code), ' ')

            try:
                messages = json.loads(content)
                web_messages = messages.__len__()
                if web_messages > 1:
                    db_messages = client.message_set.all().count()
                    db_messages_learned = client.message_set.filter(
                        learned=True).count()
                    self.stdout.write(u'{}: web={} db={} learned={}'.format(
                        client.name,
                        web_messages,
                        db_messages,
                        db_messages_learned,
                    ))
            except ValueError:
                pass
