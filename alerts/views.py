# -*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt

from .models import Resume, Client

import json
import logging

logger = logging.getLogger(__name__)
hdlr = logging.FileHandler('/var/tmp/sentimiento.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


@csrf_exempt
def group(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        items = json.loads(body['o'])
        au = body['au']
    else:
        HttpResponseServerError('no POST')

    for item in items:
        try:
            cac = item['cac'].__str__().strip('" ')
            client_code = item['ce'].strip('" ')
            expresion = item['re'].strip('" ')
            resume, created = Resume.objects.update_or_create(
                code=int(cac),
                defaults={
                    'client': Client.objects.get_code(client_code),
                    'body': expresion,
                    'classiffied': 'NEU',
                    'result': 'false',
                    'sent_at': None,
                    'security': au})

        except Exception as e:
            logger.info('Exception: ' + str(e))
            logger.info('raw item: ' + item.__str__())

    return HttpResponse('OK')
