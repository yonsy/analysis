# -*- coding: utf-8 -*-
" admin models for alerts "

from django.contrib import admin
from .models import Message, Client, Resume


class MessageAdmin(admin.ModelAdmin):
    list_display = [
        'title', 'body',
        'learned', 'classiffied',
        'date', 'time',
    ]
    list_filter = [
        'learned', 'classiffied', 'client',
    ]


class ClientAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'code',
    ]
    list_filter = [
        'state', 'service',
    ]


class ResumeAdmin(admin.ModelAdmin):
    list_display = [
        'code', 'client', 'body', 'result', 'received_at', 'sent_at',
    ]
    list_filter = [
        'classiffied', 'result', 'client',
    ]


admin.site.register(Message, MessageAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Resume, ResumeAdmin)
