# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-12 03:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alerts', '0007_resume'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resume',
            name='result',
            field=models.CharField(default=b'true', max_length=16),
        ),
    ]
