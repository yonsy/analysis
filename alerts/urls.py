# -*- coding: utf-8 -*-
from django.conf.urls import url

from views import group

urlpatterns = [
    url(r'^group/$', group, name='group'),
]
