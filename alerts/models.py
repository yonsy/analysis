# -*- coding: utf-8 -*-
"""
Messages and other models for alerts
"""

from django.db import models
from django.db.models import Q


class ClientQuerySet(models.QuerySet):

    def get_code(self, code):
        return self.get(code=code)


class MessageQuerySet(models.QuerySet):

    def learned(self, classiffied=None):
        messages = self.filter(learned=True)
        if classiffied:
            messages = messages.filter(classiffied=classiffied)
        return messages

    def not_learned(self, classiffied=None):
        messages = self.filter(learned=False)
        if classiffied:
            messages = messages.filter(classiffied=classiffied)
        return messages

    def positive(self):
        return self.filter(classiffied=Message.POSITIVE)

    def negative(self):
        return self.filter(classiffied=Message.NEGATIVE)

    def neutral(self):
        return self.filter(classiffied=Message.NEUTRAL)

    def positive_learned(self):
        return self.learned().positive()

    def negative_learned(self):
        return self.learned().negative()

    def neutral_learned(self):
        return self.learned().neutral()

    def positive_not_learned(self):
        return self.not_learned().positive()

    def negative_not_learned(self):
        return self.not_learned().negative()

    def neutral_not_learned(self):
        return self.not_learned().neutral()


class ResumeQuerySet(models.QuerySet):

    def sent(self):
        return self.filter(result=Resume.SENT)

    def not_sent(self):
        return self.filter(result=Resume.NOT_SENT)

    def in_process(self):
        return self.filter(result=Resume.IN_PROCESS)

    def in_learning(self):
        return self.filter(result=Resume.IN_LEARNING)

    def to_be_sent(self):
        return self.filter(~Q(result=Resume.SENT))


class Client(models.Model):

    name = models.CharField(
        'Nombre', max_length=64, null=False, db_index=True)
    code = models.CharField(
        'Codigo', max_length=16, null=False, unique=True, db_index=True)
    state = models.BooleanField(
        'Estado', null=False, default=False)
    service = models.BooleanField(
        'Servicio', null=False, default=False)

    objects = ClientQuerySet.as_manager()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['name', ]


class Message(models.Model):

    POSITIVE = 'POS'
    NEGATIVE = 'NEG'
    NEUTRAL = 'NEU'
    CLASIFFIED_CHOICES = (
        (POSITIVE, 'Positivo'),
        (NEGATIVE, 'Negativo'),
        (NEUTRAL, 'Neutral'),
    )

    client = models.ForeignKey(Client, verbose_name='Cliente')
    code = models.BigIntegerField('Codigo', db_index=True)
    date = models.DateField(
        'Dia', editable=False, null=False, db_index=True)
    time = models.TimeField(
        'Hora', editable=False, null=False, db_index=True)
    learned = models.BooleanField(
        'Aprendido', null=False, default=False, db_index=True)
    classiffied = models.CharField(
        'Clasificacion', null=False, max_length=3,
        choices=CLASIFFIED_CHOICES, default=NEUTRAL)
    title = models.CharField(
        'Titulo', max_length=250, null=False, db_index=True)
    body = models.TextField(
        'Contenido', max_length=16384, null=False)

    objects = MessageQuerySet.as_manager()

    class Meta:
        verbose_name = 'Mensaje aprendido'
        verbose_name_plural = 'Mensajes aprendidos'
        ordering = ['-date', '-time', ]


class Resume(models.Model):

    POSITIVE = u'POS'
    NEGATIVE = u'NEG'
    NEUTRAL = u'NEU'
    CLASIFFIED_CHOICES = (
        (POSITIVE, 'Positivo'),
        (NEGATIVE, 'Negativo'),
        (NEUTRAL, 'Neutral'),
    )

    SENT = u'true'
    NOT_SENT = u'false'
    IN_PROCESS = u'process'
    IN_LEARNING = u'learning'
    RESULT_CHOICES = (
        (SENT, 'Enviado'),
        (NOT_SENT, 'Por Enviar'),
        (IN_PROCESS, 'Procesandose para envio'),
        (IN_LEARNING, 'Procesandose para aprendizaje'),
    )

    client = models.ForeignKey(Client, verbose_name='Cliente')
    code = models.BigIntegerField('Codigo', db_index=True)
    body = models.TextField(
        'Contenido', max_length=16384, null=False)
    result = models.CharField(
        'Resultado', max_length=16, db_index=True, blank=False,
        choices=RESULT_CHOICES, default=NOT_SENT)
    received_at = models.DateTimeField(
        'Recibido', auto_now_add=True, null=False)
    sent_at = models.DateTimeField(
        'Enviado', null=True, default=None)
    classiffied = models.CharField(
        'Clasificacion', null=False, max_length=3,
        choices=CLASIFFIED_CHOICES, default=NEUTRAL, db_index=True)
    security = models.CharField(
        'Codigo de auditoria', null=True, default=None, max_length=4096)

    objects = ResumeQuerySet.as_manager()

    class Meta:
        verbose_name = 'Resumen evaluado'
        verbose_name_plural = 'Resumenes evaluados'
        ordering = ['-received_at', '-sent_at']
